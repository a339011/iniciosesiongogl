import express from '/express';

const app = express();

app.get('/', (request, response)=>{
    response.send("Hola!!!");
}
);

app.post('/', (request, response)=>{
    response.send("Hola desde Post");
});

app.put('/', (request, response)=>{
    response.send("Hola desde Put");
});

app.patch('/', (request, response)=>{
    response.send("Hola desde Patch");
});

app.delete('/', (request, response)=>{
    response.send("Hola desde Delete");
});


function handleCredentialResponse(response) {
    // decodeJwtResponse() is a custom function defined by you
    // to decode the credential response
    
    const responsePayload = decodeJwtResponse(response.credential);

    console.log("ID: " + responsePayload.sub);
    console.log('Full Name: ' + responsePayload.name);
    console.log('Given Name: ' + responsePayload.given_name);
    console.log('Family Name: ' + responsePayload.family_name);
    console.log("Image URL: " + responsePayload.picture);
    console.log("Email: " + responsePayload.email);

    //TODO: Checar que el correo termine en arrobauach
    //TODO: Que si inicia sesión correctamente, la vista sea ahora menu.html (si existe, ya ta en la carpeta raiz)
 }

